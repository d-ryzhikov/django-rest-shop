from operator import attrgetter

from rest_framework.serializers import Serializer, ListSerializer


class RecursiveField(Serializer):
    def __init__(self, *args, **kwargs):
        self._max_depth = kwargs.pop('max_depth', None)
        self._many = kwargs.pop('many', False)
        super(RecursiveField, self).__init__(*args, **kwargs)

    def to_representation(self, instance):
        if isinstance(self.parent, ListSerializer):
            parent = self.parent.parent
        else:
            parent = self.parent
        serializer_class = parent.__class__

        current_depth = getattr(parent, '_current_depth', 1)

        max_depth = getattr(parent, '_max_depth', None)
        if max_depth is None:
            if self.context['request'].query_params.get('max_depth'):
                max_depth = int(self.context['request'].query_params.get('max_depth'))
            else:
                max_depth = getattr(self, '_max_depth', None)

        if max_depth is None or current_depth < max_depth:
            serializer = serializer_class(instance, context=self.context,
                                          many=self._many)
            serializer._max_depth = max_depth
            serializer._current_depth = current_depth + 1
            return serializer.data
        else:
            return '...'


class ManyToManyAsObject(Serializer):
    def __init__(self, *args, **kwargs):
        self.keys_source = kwargs.pop('keys_source')
        self.values_source = kwargs.pop('values_source')
        self.keys_getter = attrgetter(self.keys_source)
        self.values_getter = attrgetter(self.values_source)
        super(ManyToManyAsObject, self).__init__(*args, **kwargs)

    def to_representation(self, many_to_many):
        res = {}
        for prop in many_to_many.all():
            res[self.keys_getter(prop)] = self.values_getter(
                many_to_many.through.objects.get(
                    **{
                        many_to_many.source_field_name: many_to_many.instance,
                        many_to_many.target_field_name: prop
                    }
                )
            )
        return res
