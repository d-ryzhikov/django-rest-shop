const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const base_config = require('./webpack.config.base');

module.exports = {
    ...base_config,

    plugins: [
        ...base_config.plugins,
        new UglifyJsPlugin()
    ],

    output: {
        ...base_config.output,
        filename: '[name]-[hash].js'
    }
};
