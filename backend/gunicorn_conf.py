import multiprocessing
import os


def when_ready(server):
    fname = '/tmp/app-initialized'
    fhandle = open(fname, 'a')
    try:
        os.utime(fname, None)
    finally:
        fhandle.close()


bind = 'unix:///tmp/nginx.socket'
workers = 2 * multiprocessing.cpu_count() + 1
