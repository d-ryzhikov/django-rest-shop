from django.contrib import admin

from .models import Category, Product, Property, SKU, SKUProperty

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Property)
admin.site.register(SKU)
admin.site.register(SKUProperty)
