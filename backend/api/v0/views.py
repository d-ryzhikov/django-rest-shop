from rest_framework.viewsets import ReadOnlyModelViewSet

from shop.models import Category, Product, SKU
from .serializers import (
    CategoryDetailSerializer,
    ProductDetailSerializer, ProductInlineSerializer,
    SKUDetailSerializer, SKUInlineSerializer,
)


class CategoryViewSet(ReadOnlyModelViewSet):
    serializer_class = CategoryDetailSerializer

    def get_queryset(self):
        if self.action == 'list':
            # list only root categories
            return Category.objects.filter(parent__isnull=True)
        return Category.objects.all()


class ProductViewSet(ReadOnlyModelViewSet):
    queryset = Product.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return ProductInlineSerializer
        elif self.action == 'retrieve':
            return ProductDetailSerializer


class SKUViewSet(ReadOnlyModelViewSet):
    queryset = SKU.objects.all()
    serializer_class = SKUDetailSerializer

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'list':
            return SKUInlineSerializer
        elif self.action == 'retrieve':
            return SKUDetailSerializer
