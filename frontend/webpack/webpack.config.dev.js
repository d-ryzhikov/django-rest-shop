const base_config = require('./webpack.config.base');

module.exports = {
    ...base_config,

    output: {
        ...base_config.output,
        filename: 'bundle.js'
    },

    watch: true,

    devServer: {
        socket: '/tmp/webpack.socket',
        hot: true
    }
};
