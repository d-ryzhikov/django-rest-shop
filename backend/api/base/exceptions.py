from rest_framework.exceptions import APIException
from rest_framework import status


class APIVersionDeprecated(APIException):
    status_code = status.HTTP_410_GONE
