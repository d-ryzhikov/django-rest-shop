# Django-rest-shop

Demo project to learn Django Rest Framework, Docker, React.js and Redux.

================================================================================

## Dependencies:
 - Docker

## Project start-up
```
docker-compose up
```
