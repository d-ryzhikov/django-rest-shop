#!/bin/bash

APP_PATH="/home/ubuntu/django-rest-shop"
BACKEND_PATH="${APP_PATH}/backend"
FRONTEND_PATH="${APP_PATH}/frontend"
NGINX_PATH="${APP_PATH}/nginx"

cd ${APP_PATH}

# add postgres repo
add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt-get update
apt-get install build-essential

# postgres installation
apt-get install -y postgresql-9.6
su postgres -c "psql -c \"DROP DATABASE IF EXISTS drs\" "
su postgres -c "psql -c \"DROP ROLE IF EXISTS drs\" "
su postgres -c "psql -c \"CREATE ROLE drs SUPERUSER LOGIN PASSWORD 'password'\" "
su postgres -c "psql -c \"CREATE DATABASE drs OWNER = drs\" "

# nginx installation and config creation
cp ${NGINX_PATH}/nginx.service /etc/systemd/system/nginx.service
apt-get install -y ruby
apt-get install -y nginx
export $(cat ${APP_PATH}/.env | xargs)  # load env vars
erb "${NGINX_PATH}/config/nginx.conf.erb" > /etc/nginx/nginx.conf

# python virtualenv
VIRTUAL_ENV_PATH="${BACKEND_PATH}/.venv"

apt-get install -y python-pip
pip install -U pip
pip install -U virtualenv
virtualenv ${VIRTUAL_ENV_PATH} -p /usr/bin/python3
source "${VIRTUAL_ENV_PATH}/bin/activate"
pip install -r "${BACKEND_PATH}/requirements.txt"
deactivate

# npm installation
cd ${FRONTEND_PATH}
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get install -y nodejs
apt-get install -y npm
npm install
