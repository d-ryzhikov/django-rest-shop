from django.conf import settings
from django.conf.urls import include, url

app_name = 'api'

urlpatterns = [
    url(r'^v(?P<version>0\.\d+\.\d+)/', include('api.v0.urls')),

    url(
        regex=r'^v(?P<version>\d+\.\d+\.\d+)/',
        view=include(settings.API_LATEST_URLCONF)
    ),
]
