# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-22 15:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sku',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='skus', to='shop.Product'),
        ),
        migrations.AlterField(
            model_name='skuproperty',
            name='property',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sku_properties', to='shop.Property'),
        ),
        migrations.AlterField(
            model_name='skuproperty',
            name='sku',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sku_properties', to='shop.SKU'),
        ),
    ]
