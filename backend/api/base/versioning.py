from django.conf import settings

from rest_framework.compat import unicode_http_header
from rest_framework.exceptions import NotFound
from rest_framework.versioning import BaseVersioning
from rest_framework.utils.mediatypes import _MediaType

from .exceptions import APIVersionDeprecated


class UrlAcceptHeaderVersioning(BaseVersioning):
    def determine_version(self, request, *args, **kwargs):
        """Version determinition priority:
        1) url
        2) Accept header
        3) default version
        """
        # try to get version from url
        version = kwargs.get('version')

        if not version:
            # if version was not specified in url, try to retrieve it from
            # Accept request header
            media_type = _MediaType(request.accepted_media_type)
            version = media_type.params.get(
                self.version_param, self.default_version)
            version = unicode_http_header(version)

        if not version:
            # if neither url nor Accept header specified a version,
            # return the default one
            return self.default_version

        if not self.is_allowed_version(version):
            if version in settings.API_DEPRECATED_VERSIONS:
                raise APIVersionDeprecated(
                    '{version!s} version of API '
                    'for {path!s} is DEPRECATED.'.format(
                        version=version, path=request.path)
                )
            else:
                raise NotFound(
                    '{version!s} version of API '
                    'for {path!s} is not found.'.format(
                        version=version, path=request.path)
                )

        return version

    def reverse(
            self, viewname, args=None, kwargs=None, request=None,
            format=None, **extra):
        if request.version is not None:
            kwargs = {} if (kwargs is None) else kwargs
            kwargs[self.version_param] = request.version

        return super(UrlAcceptHeaderVersioning, self).reverse(
            viewname, args, kwargs, request, format, **extra)
