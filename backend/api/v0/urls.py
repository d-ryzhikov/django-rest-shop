from rest_framework.routers import DefaultRouter

from .views import CategoryViewSet, ProductViewSet, SKUViewSet

router = DefaultRouter()
router.register(r'categories', CategoryViewSet, base_name='category')
router.register(r'products', ProductViewSet, base_name='product')
router.register(r'skus', SKUViewSet, base_name='sku')

urlpatterns = router.urls
