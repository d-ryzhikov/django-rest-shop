#!/usr/bin/env bash
export PYTHON_PATH="/app/backend/.heroku/python/bin"
export PYTHON="${PYTHON_PATH}/python3"
export GUNICORN="${PYTHON_PATH}/gunicorn"
export BACKEND_PATH="/app/backend"

${PYTHON} "${BACKEND_PATH}/manage.py" collectstatic --noinput
cd nginx
bin/start-nginx ${PYTHON} ${GUNICORN} -c "${BACKEND_PATH}/gunicorn_conf.py" \
    --chdir "${BACKEND_PATH}" \
    django_rest_shop.wsgi:application
