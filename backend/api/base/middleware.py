import re

from django.conf import settings


class AcceptHeaderVersionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.url_version_regex = re.compile(
            r'^/api/v\d+\.\d+\.\d+/.*$'
        )
        self.header_version_regex = re.compile(
            r'version=(?P<version>\d+\.\d+\.\d+)'
        )

    def __call__(self, request):
        if (request.path.startswith('/api/')
                and not self.url_version_regex.match(request.path)):
            version = (
                self.retrieve_header_version(request.META['HTTP_ACCEPT'])
                or settings.REST_FRAMEWORK['DEFAULT_VERSION']
            )
            request.path_info = request.path.replace(
                '/api/', '/api/v{}/'.format(version), 1
            )

        return self.get_response(request)

    def retrieve_header_version(self, header):
        match = self.header_version_regex.search(header)
        if match:
            return match.groupdict()['version']
