from rest_framework import serializers

from shop.models import Category, Product, SKU
from api.base.fields import ManyToManyAsObject, RecursiveField


class CategoryDetailSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api:category-detail', read_only=True)
    subcategories = RecursiveField(many=True)

    class Meta:
        model = Category
        fields = ('id', 'url', 'name', 'subcategories')


class CategoryInlineSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api:category-detail', read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'url', 'name')


class ProductInlineSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api:product-detail', read_only=True)
    category = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'url', 'category')


class SKUDetailSerializer(serializers.HyperlinkedModelSerializer):
    properties = ManyToManyAsObject(keys_source='name', values_source='value')
    product = ProductInlineSerializer()

    class Meta:
        model = SKU
        fields = ('id', 'SKU', 'price', 'quantity', 'properties', 'product')


class SKUInlineSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='api:sku-detail')

    class Meta:
        model = SKU
        fields = ('SKU', 'url')


class ProductDetailSerializer(serializers.HyperlinkedModelSerializer):
    category = CategoryInlineSerializer()
    skus = SKUInlineSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'skus')
