from django.db import models


class Category(models.Model):
    parent = models.ForeignKey(
        'Category', related_name='subcategories', null=True, blank=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey('Category')

    def __str__(self):
        return self.name


class SKU(models.Model):
    product = models.ForeignKey('Product', related_name='skus')
    SKU = models.CharField(max_length=100, unique=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    quantity = models.PositiveIntegerField()
    properties = models.ManyToManyField(
        'Property', through='SKUProperty', related_name='skus')

    def __str__(self):
        return self.SKU


class Property(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class SKUProperty(models.Model):
    sku = models.ForeignKey('SKU', related_name='sku_properties')
    property = models.ForeignKey('Property', related_name='sku_properties')
    value = models.CharField(max_length=150)
