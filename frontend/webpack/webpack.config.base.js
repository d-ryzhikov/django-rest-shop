const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: __dirname,

    entry: '../src/js/index.jsx',

    output: {
        path: path.resolve(__dirname, '../public/'),
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: '../src/html/index.html',
            inject: 'body'
        })
    ],

    module: {
        loaders: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-3']
                }
            }
        ]
    },

    resolve: {
        extensions: ['.js', '.jsx']
    }
};
